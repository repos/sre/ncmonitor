"""
SPDX-License-Identifier: GPL-3.0-or-later
"""
from unittest import TestCase, mock
import json

from ncmonitor.ncmonitor import NCMonitorConfig, SetToListEncoder, get_domains, process_domains

CONFIG = """---
acmechief:
  conf-path: hieradata/common/certificates.yaml
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
dnsrepo:
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/dns
  target-zone-path: templates/ncredir-parking
gerrit:
  reviewers:
  - user1@example.com
  - user2@example.com
  ssh-key-path: "/etc/ncmonitor/gerrit.key"
markmonitor:
  username: genesisdoes
  password: whatnintendont
  ignored-domains:
    - "wikimedia.cloud"
    - "wikimediacloud.org"
nameservers:
- ns1.example.com
- ns2.example.com
ncredir:
  datfile-path: modules/ncredir/files/nc_redirects.dat
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
suffix-list-path: "tests/test_public_suffix_list.dat"
"""

DOMAINS_ACME = {
    "wikimedia.org",
    "wikiversity.org",
    "wikiduplicate.org",
}
DOMAINS_DNSREPO = {
    "wikimedia.org",
    "wiktionary.org",
    "wikiduplicate.org",
}
DOMAINS_MARK = {
    "wikidata.org": {
        "nameservers": ["ns1.externalnssite.com"],
        "status": "registered locked",
    },
    "wikifunctions.org": {
        "nameservers": None,
        "status": "registered locked"
    },
    "wikimedia.cloud": {
        "nameservers": ["ns1.externalssite.com"],
        "status": "hold",
    },
    "wikisource.org": {
        "nameservers": ["ns1.example.com"],
        "status": "hold",
    },
    "wikimedia.org": {
        "nameservers": ["ns1.example.com", "ns2.example.com"],
        "status": "registered locked",
    },
    "wikivoyage.org": {
        "nameservers": ["ns1.example.com", "ns9.example.com"],
        "status": "registered locked",
    },
    "wikiduplicate.org": {
        "nameservers": ["ns1.example.com", "ns2.example.com"],
        "status": "duplicate",
    },
}
DOMAINS_NCREDIR = {
    "subdomain.wikimedia.org",
    "subdomain.wikimediacloud.org",
    "subdomain.wikiquote.org",
    "wikiduplicate.org",
    "wikimedia.org",
    "wikiquote.org",
}

DOMAINS_CAT = {
    "acme": DOMAINS_ACME,
    "dnsrepo": DOMAINS_DNSREPO,
    "mark": DOMAINS_MARK,
    "ncredir": DOMAINS_NCREDIR,
}

EXPECTED_PROCESSING_RESULT = {
    "acme": {
        "missing": ["wikivoyage.org"],
        "unused": ["wikiversity.org"],
    },
    "dnsrepo": {
        "missing": ["wikivoyage.org"],
        "unused": ["wiktionary.org"],
    },
    "mark": {
        "empty_ns": ["wikifunctions.org"],
        "wrong_ns": ["wikivoyage.org"],
        "unmanaged": ["wikisource.org"],
        "duplicate": ["wikiduplicate.org"],
    },
    "ncredir": {
        "missing": ["wikivoyage.org"],
        "unused": ["subdomain.wikiquote.org", "wikiquote.org"],
    },
}


def sort_results(results: str) -> dict:
    """
    Sort any and all lists
    """
    results_json = json.loads(results)
    for svc, result in results_json.items():
        for key, value in result.items():
            results_json[svc][key] = sorted(value)
    return results_json

class ResultsTest(TestCase):
    def setUp(self):
        # Config with proper file permissions
        self.mock_os_stat_patch = mock.patch("os.stat")
        self.mock_os_stat = self.mock_os_stat_patch.start()
        self.mock_os_stat.return_value.st_mode = 33184  # 0640

        self.mock_builtins_open_patch = mock.patch("builtins.open",
                                                   mock.mock_open(read_data=CONFIG))
        self.mock_builtins_open = self.mock_builtins_open_patch.start()

        self.config = NCMonitorConfig(CONFIG)

        self.mock_get_domains_patch = mock.patch("ncmonitor.ncmonitor.get_domains")
        self.mock_get_domains = self.mock_get_domains_patch.start()
        self.mock_get_domains.return_value = DOMAINS_CAT
        # Assertion errors are trunctated by default and our JSON is long
        self.maxDiff = None

    def tearDown(self):
        self.mock_get_domains_patch.stop()

    def test_domain_processing(self):
        domains = self.mock_get_domains("_", "_")
        actual_results = process_domains(self.config.nameservers, domains,
                                         self.config.config_file["suffix-list-path"],
                                         self.config.config_file["markmonitor"]["ignored-domains"])

        # This should be completely ignored because it doesn't use our nameservers
        self.assertNotIn("wikidata.org", actual_results)

        actual_results = sort_results(actual_results)
        self.assertEqual(actual_results, EXPECTED_PROCESSING_RESULT)
