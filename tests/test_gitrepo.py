"""
SPDX-License-Identifier: GPL-3.0-or-later
"""

from pathlib import Path
import re
import tempfile
import unittest
from unittest import mock

from git import Repo

from ncmonitor.gitrepo import (
    ACMEChiefConfig,
    DNSRepository,
    GitRepository,
    GitRepositoryError,
    NCRedirRedirects,
)

ACMECHIEF_CONF_PATH = "hieradata/common/certificates.yaml"
ACMECHIEF_CONF_CONTENTS = r"""certificates::acme_chief:
  non-canonical-redirect-1:
    CN: wikinews.org
    SNI:
    - wikivoyage.gr
    - wikipedi.org
    - wikimediastories.org
    - m-wikipedia.org
    - transparency.wiki
    - wikipdiamovement.org
    - aboutwikipedia.org
    - wikiedia.org
    - wikinotifications.org
    - wikymedia.org
    - wikimedias.org
    - wikipediagroup.net
    - wikibooks.ee
    - wikimobipedia.net
    - wikipediya.org
    - frwikipedia.org
    - functions.wiki
    - wikipediia.net
    - wikipedia.cloud
    - wikipediapublisher.com
    - wikibooks.us
    - wikinews.info
    - wikiversity.pl
    - wikipediawriters.com
    - wikiartpedia.mobi
    - wikiartpedia.info
    - wikipedia.foundation
    - ikipedia.org
    - wikioedia.org
    - wikipedia.pt
    - movement-strategy.org
    - wiikpedia.org
    - wikinews.ee
    - wikimedija.com
    - onwikipedia.com
    - wikinews.gr
    - wikipedia.sucks-block
    - wikipediastatus.org
    - wikimediauk.biz
    staging_time: 604800
    challenge: dns-01
    authorized_regexes:
    - '^ncredir[0-9]{4,}\..*\.wmnet$'
    prevalidate: true
    skip_invalid_snis: true
"""

DNSREPO_CONF_PATH = "templates/ncredir-parking"

NC_CONF_PATH = "modules/ncredir/files/nc_redirects.dat"
NC_CONF_CONTENTS = """# Test comment
funnel	wikimedia.community	https://www.wikimedia.org
funnel	*wikimedia.us	https://www.wikimedia.org
rewrite	*mediawiki.com	https://www.mediawiki.org
rewrite	*.voyagewiki.org	https://*.wikivoyage.org
rewrite	voyagewiki.com	https://www.wikivoyage.org
"""

class GitRepositoryTest(unittest.TestCase):
    @mock.patch.object(Repo, 'clone_from')
    def test_clone(self, clone_mock):
        with GitRepository('https://fake.git.server/test.git') as git_clone:
            clone_mock.assert_called_once_with('https://fake.git.server/test.git',
                                               git_clone.temp_directory.name, depth=1)

class GitRepositoryManipulation(unittest.TestCase):
    def setUp(self):
        # Create a local Git repo simulation so we don't have to remotely clone
        # pylint: disable=consider-using-with
        self.temp_directory = tempfile.TemporaryDirectory(prefix="ncmonitor_test_")
        self.repo = Repo.init(self.temp_directory.name)

        for item in zip([DNSREPO_CONF_PATH, NC_CONF_PATH, ACMECHIEF_CONF_PATH],
                        ["/dev/null", NC_CONF_CONTENTS, ACMECHIEF_CONF_CONTENTS]):
            full_path = Path(self.temp_directory.name).joinpath(item[0])
            full_path.parent.mkdir(parents=True, exist_ok=True)
            with open(full_path, mode="w", encoding="utf-8") as file:
                file.write(item[1])
            self.repo.index.add(item[0])

        self.repo.index.commit("Initial commit for test suites")
        assert not self.repo.is_dirty()

    def tearDown(self):
        self.temp_directory.cleanup()

    def test_dnsrepo_add_rm_entry(self):
        with DNSRepository(self.temp_directory.name, DNSREPO_CONF_PATH) as dnsrepo:
            dnsrepo.add_entries(["a.example.com", "a.b.example.com", "abc.example.com",
                                 "abcd.example.com", "abcde.example.com"])

            # Domains that should have been automatically dropped.
            self.assertFalse(dnsrepo.zonefile_dir_full.joinpath(
                "wikipedia.sucks-block").is_symlink()
            )
            self.assertTrue(dnsrepo.zonefile_dir_full.joinpath("a.example.com").is_symlink())
            self.assertTrue(dnsrepo.zonefile_dir_full.joinpath("a.b.example.com").is_symlink())
            self.assertTrue(dnsrepo.zonefile_dir_full.joinpath("abc.example.com").is_symlink())

            dnsrepo.remove_entries(["a.example.com", "a.b.example.com"])
            self.assertFalse(dnsrepo.zonefile_dir_full.joinpath("a.example.com").is_symlink())
            self.assertFalse(dnsrepo.zonefile_dir_full.joinpath("a.b.example.com").is_symlink())

            dnsrepo.add_entries(["c.example.com"])
            dnsrepo.commit()

    def test_ncredir_add_rm_entry(self):
        with NCRedirRedirects(self.temp_directory.name, NC_CONF_PATH) as ncredir:
            ncredir.add_entries(["example.com"])
            # Added entries should manifest with a prepended asterisk, the
            # syntax for including both the base domain as well as all
            # subdomains.
            self.assertIn("rewrite\t*example.com\thttps://www.wikimedia.org", ncredir.working_file)

            ncredir.write_file()
            ncredir.commit()

            # Pretend like variants of the same domain are in the file with
            # manual insertions. All variants should be removed with
            # remove_entries().
            ncredir.working_file.append("rewrite\texample.com\thttps://example.com")
            ncredir.working_file.append("rewrite\t*.example.com\thttps://example.com")
            ncredir.remove_entries(["example.com"])
            self.assertNotIn("rewrite\texample.com\thttps://www.wikimedia.org",
                             ncredir.working_file)
            self.assertNotIn("rewrite\t*example.com\thttps://www.wikimedia.org",
                             ncredir.working_file)
            self.assertNotIn("rewrite\t*.example.com\thttps://www.wikimedia.org",
                             ncredir.working_file)

    def test_acmechief_add_rm_entry(self):
        with ACMEChiefConfig(self.temp_directory.name, ACMECHIEF_CONF_PATH) as acmechief:

            self.assertTrue("wikipedia.sucks-block" in ACMECHIEF_CONF_CONTENTS)
            self.assertFalse("wikipedia.sucks-block" in acmechief.active_cert_block)

            # There's still room here
            self.assertEqual(acmechief.active_cert_block, "non-canonical-redirect-1")

            acmechief.add_entries(["example1.com", "example2.com", "example3.com"])

            # We should have switched over to a new block mid-insert.
            self.assertEqual(acmechief.active_cert_block, "non-canonical-redirect-2")

            self.assertEqual(acmechief.working_file["certificates::acme_chief"]
                                                   [acmechief.active_cert_block]["CN"],
                             "example1.com")
            self.assertEqual(acmechief.working_file["certificates::acme_chief"]
                                                   [acmechief.active_cert_block]["CN"],
                             "example1.com")
            # The CN should still be in the SNI list
            self.assertIn("example1.com",
                          acmechief.working_file["certificates::acme_chief"]
                                                [acmechief.active_cert_block]["SNI"])

            # We should also have subdomains added
            self.assertIn("*.example1.com",
                          acmechief.working_file["certificates::acme_chief"]
                                                [acmechief.active_cert_block]["SNI"])

            acmechief.remove_entries(["example1.com"])
            self.assertNotIn("example1.com",
                             acmechief.working_file["certificates::acme_chief"]
                                                   [acmechief.active_cert_block]["SNI"])
            # We should also have removed its subdomain wildcard
            self.assertNotIn("*.example1.com",
                             acmechief.working_file["certificates::acme_chief"]
                                                   [acmechief.active_cert_block]["SNI"])

            acmechief.write_file()
            acmechief.commit()

            # TODO: Test removal of entry that's a CN

    def test_commit_to_clean_repo(self):
        with NCRedirRedirects(self.temp_directory.name, NC_CONF_PATH) as ncredir:
            with self.assertRaises(GitRepositoryError):
                ncredir.commit()
