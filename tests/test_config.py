"""
SPDX-License-Identifier: GPL-3.0-or-later
"""

from unittest import TestCase, mock

from ncmonitor.config import NCConfigError, NCMonitorConfig, is_valid_hostname

# FIXME: Refactor this whole test suite - iterate through a whole "good" file:
# Strip out a section, test that it errors out, then strip another part out,
# etc.
TEST_CONFIG_GOOD_NS = """---
acmechief:
  conf-path: hieradata/common/certificates.yaml
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
dnsrepo:
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/dns
  target-zone-path: templates/ncredir-parking
gerrit:
  reviewers:
  - user1@example.com
  - user2@example.com
  ssh-key-path: "/etc/ncmonitor/gerrit.key"
markmonitor:
  username: genesisdoes
  password: whatnintendont
nameservers:
- example.com
- "example.org"
- example
- www.example.com
- ns1.exa.mple.com
ncredir:
  datfile-path: modules/ncredir/files/nc_redirects.dat
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
suffix-list-path: "placeholder"
"""

TEST_CONFIG_BAD_NS = """---
acmechief:
  conf-path: hieradata/common/certificates.yaml
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
dnsrepo:
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/dns
  target-zone-path: templates/ncredir-parking
gerrit:
  reviewers:
  - user1@example.com
  - user2@example.com
  ssh-key-path: "/etc/ncmonitor/gerrit.key"
markmonitor:
  username: genesisdoes
  password: whatnintendont
nameservers:
  - a@.com
  - a
  - example.
ncredir:
  datfile-path: modules/ncredir/files/nc_redirects.dat
  remote-url: ssh://ncmonitor@gerrit.wikimedia.org:29418/operations/puppet
suffix-list-path: "placeholder"
"""


class ConfigTest(TestCase):
    def setUp(self):
        # Config with proper file permissions
        self.mock_os_stat_patch = mock.patch("os.stat")
        self.mock_os_stat = self.mock_os_stat_patch.start()
        self.mock_os_stat.return_value.st_mode = 33184  # 0640

    def tearDown(self):
        self.mock_os_stat_patch.stop()

    @mock.patch("builtins.open", mock.mock_open(read_data=TEST_CONFIG_BAD_NS))
    def test_load_config_bad(self):
        self.assertRaises(NCConfigError, NCMonitorConfig)

    @mock.patch("builtins.open", mock.mock_open(read_data=TEST_CONFIG_GOOD_NS))
    def test_load_config_good(self):
        conf = NCMonitorConfig()
        self.assertTrue(conf.config_file)  # Make sure we're not empty
        self.assertTrue("example.com" in conf.nameservers)
        self.assertEqual(conf.config_file["markmonitor"]["username"], "genesisdoes")
        self.assertEqual(conf.config_file["markmonitor"]["password"], "whatnintendont")

    # Patch builtins.open with a good config so cryptic errors don't manifest
    # if this test fails.
    @mock.patch("builtins.open", mock.mock_open(read_data=TEST_CONFIG_GOOD_NS))
    @mock.patch("os.stat")
    def test_config_exit_invalid_mode(self, mock_os_stat):
        mock_os_stat.return_value.st_mode = 33188  # 0644
        self.assertRaises(NCConfigError, NCMonitorConfig)
