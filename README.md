# ncmonitor

Wikimedia manages a large amount of [domains](https://wikitech.wikimedia.org/wiki/Domains). Registration and maintenance is time-consuming; This project automates these difficulties.

## Requirements

ncmonitor was developed to track the latest Debian stable release, so the
Python versions will be what ships in Debian. For more detailed information
about other dependencies see the pyproject.toml and tox.ini files.

## Installation

ncmonitor uses a [PEP 518](https://peps.python.org/pep-0518/)-based
pyproject.toml file using the setuptools backend. You may use your typical
setuptools interface to install, such as pip:

```bash
$ pip install .
```

## Configuration

View ncmonitor(5) for information on configuration.
