"""
SPDX-License-Identifier: GPL-3.0-or-later
"""

from argparse import ArgumentParser
from collections import defaultdict
from email.message import EmailMessage
import json
import os
import logging
from typing import Tuple

from pathlib import Path
from smtplib import SMTP
from socket import getfqdn
from tldextract import TLDExtract
from tldextract.suffix_list import SuffixListNotFound

from .config import NCMonitorConfig
from .gitrepo import DNSRepository, ACMEChiefConfig, NCRedirRedirects
from .markmonitor import MarkMonitor


logger = logging.getLogger(__name__)
MM_BLOCK_SUFFIX = "-block"
# https://corp.markmonitor.com/domains/setup/restapi/#/Domain/findDomainById
MM_ALLOWED_STATUSES = [
    "accepted",
    "pending update",
    "abandoned",  # Still managed by MM at this stage
    "registered locked",
    "registered unlocked",
    "registrar hold",
    "registered premium locked",
    "registered super locked",
]


class SetToListEncoder(json.JSONEncoder):
    """
    JSON encoder that handles serialization of sets.
    """
    # pylint: disable-next=arguments-renamed
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


def get_conf_path() -> Path:
    """
    Locate user-supplied configuration file or check in pre-configured paths.
    """
    conf_name = Path("ncmonitor.yaml")
    default_paths = [
        Path(Path.cwd() / conf_name),
        Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config" / "ncmonitor" / conf_name)),
        Path("/etc/ncmonitor" / conf_name),
    ]
    for candidate in default_paths:
        if isinstance(candidate, Path) and candidate.is_file():
            return candidate

    logger.exception("Unable to locate any configuration file.\nSearched in %s", default_paths)
    raise FileNotFoundError


def parse_args():
    """
    Parse runtime arguments passed in.
    """
    parser = ArgumentParser(
        description="Look up registrar DNS values and sync them with other services."
    )
    parser.add_argument("-c", "--config", type=str, help="load specified configuration file")
    parser.add_argument("-d", "--debug", action="store_true", help="Show debugging output")
    parser.add_argument("--dry-run", action="store_true", help="Perform trial run with no changes")
    parser.add_argument("-e", "--email-ns-issues", action="store_true",
                        help="Email recipients a list of domains with empty/wrong NS records")
    parser.add_argument("-s", "--submit-patch", action="store_true", help="Submit patch for review")
    parser.add_argument("-p", "--pprint", action="store_true", help="Pretty-print JSON output")
    parser.add_argument("-v", "--verbose", action="store_true", help="Show verbose output")
    return parser.parse_args()


def setup_logging(args) -> None:
    """
    Configure the runtime according to passed arguments and the configuration
    file.
    """
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)


# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def get_domains(mark_user: str,
                mark_pass: str,
                acme: ACMEChiefConfig,
                ncredir: NCRedirRedirects,
                dnsrepo: DNSRepository,
                pretty_print: bool = False) -> dict:
    """
    Reach out, populate, and return domains from each service. MarkMonitor's
    entries are different from the others: Rather than just a list of domains,
    it features a list of definitions. Each domain has a value containing its
    nameservers.
    """
    domains = defaultdict(set)  # type: ignore
    domains["mark"] = defaultdict(dict)

    with MarkMonitor(mark_user, mark_pass) as mark:
        for domain in mark.get_domain_list():
            # Services such as GlobalBlock allow us to prevent others from
            # registering some domain names that use our trademarks. They're
            # not legitimate domains. This is put here instead of
            # get_domain_list() because it feels deceiving to a method that
            # supposedly returns everything from MarkMonitor.
            if MM_BLOCK_SUFFIX not in domain.domain_name:
                if domains["mark"][domain.domain_name]:
                    logger.debug("%s is a duplicate domain. dropping", domain.domain_name)
                    domains["mark"][domain.domain_name] = {
                        "nameservers": None,
                        "status": "duplicate",
                    }
                else:
                    domains["mark"][domain.domain_name] = {
                        "nameservers": domain.nameservers,
                        "status": domain.status,
                    }
    logger.info("Fetched %s domains from MarkMonitor", len(domains["mark"]))

    for domain in acme.get_domain_list():
        domains["acme"].add(domain.domain_name)

    for domain in ncredir.get_domain_list():
        domains["ncredir"].add(domain.domain_name)

    for domain in dnsrepo.get_domain_list():
        domains["dnsrepo"].add(domain.domain_name)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Collected list of domains from all services:")
        if pretty_print:
            logger.debug(json.dumps(domains, sort_keys=True, cls=SetToListEncoder, indent=4))
        else:
            logger.debug(json.dumps(domains, sort_keys=True, cls=SetToListEncoder))

    for svc in ["acme", "ncredir", "dnsrepo"]:
        logger.info("Found %s domains in %s repository", len(domains[svc]), svc)

    return domains


# TODO: Clean this up
# pylint: disable-next=too-many-statements,too-many-branches,too-many-locals
def process_domains(nameservers: list[str], domains: dict, suffix_list_path: str,
                    ignored_domains: list[str], pretty_print: bool = False) -> str:
    """
    Compare and output domain inconsistencies in JSON.
    """
    # MarkMonitor is our definitive list of domains; All other services are
    # compared against MarkMonitor's list. "Valid" domains in MarkMonitor are
    # therefore judged a little differently: Rather than comparing whether the
    # domains are also in some other list, we just need to verify if the
    # nameservers match what we expect.
    results = defaultdict(dict)  # type: ignore
    results["mark"]["wrong_ns"] = set()
    results["mark"]["empty_ns"] = set()
    results["mark"]["unmanaged"] = set()
    results["mark"]["duplicate"] = set()

    managed_domains = set()
    for domain, domain_info in domains["mark"].items():
        if domain in ignored_domains:
            logger.debug("%s configured in the ignorelist. Ignoring.", domain)
            continue

        if domain_info["status"] == "duplicate":
            logger.debug("Multiple domains in MarkMonitor named %s; Ignoring this domain.",
                         domain)
            results["mark"]["duplicate"].add(domain)
            ignored_domains.append(domain)
            continue

        domain_nses = domain_info["nameservers"]

        if domain_info["status"] not in MM_ALLOWED_STATUSES:
            logger.debug("%s has a status of '%s'; The domain is not ours.",
                         domain, domain_info["status"])
            results["mark"]["unmanaged"].add(domain)
            continue

        # If there are any of WMF's NSes already set we'll assume we manage
        # them and just have a misconfiguration. We won't bother with any
        # domains that don't use our nameserveres: We don't need to assign
        # redirects or certificates if we're not managing them.
        if not domain_nses:
            logger.debug("%s has no nameservers assigned. Ignoring this domain.", domain)
            results["mark"]["empty_ns"].add(domain)
            ignored_domains.append(domain)
            continue

        # Remove any potential trailing periods to match up.
        domain_nses = [d.rstrip('.') for d in domain_nses]

        if not [ns for ns in nameservers if ns in domain_nses]:
            logger.debug("%s uses non-WMF nameservers: %s. Ignoring.", domain, domain_nses)
            ignored_domains.append(domain)
            continue

        managed_domains.add(domain)
        if sorted(domain_nses) != sorted(nameservers):
            logger.debug("%s has unmatching nameservers: %s", domain, domain_nses)
            results["mark"]["wrong_ns"].add(domain)
            continue

    results["acme"]["missing"] = managed_domains - domains["acme"]
    results["dnsrepo"]["missing"] = managed_domains - domains["dnsrepo"]
    results["ncredir"]["missing"] = managed_domains - domains["ncredir"]

    # Duplicates shouldn't be removed; They might still be active.
    results["acme"]["unused"] = domains["acme"] - set(ignored_domains) - \
        (managed_domains | results["mark"]["duplicate"])
    results["dnsrepo"]["unused"] = domains["dnsrepo"] - set(ignored_domains) - \
        (managed_domains | results["mark"]["duplicate"])

    # ncredir has subdomains, so we need to figure out what's in use
    # differently than other services.
    try:
        # TLDExtract() doesn't like relative paths, which is useful for tests.
        suffix_list_path_full = Path(suffix_list_path).resolve()
        tld_extractor = TLDExtract(suffix_list_urls=[f"file://{suffix_list_path_full}"],
                                   include_psl_private_domains=True,
                                   cache_dir=None, fallback_to_snapshot=False)
        assert tld_extractor.tlds
    except SuffixListNotFound as err:
        logger.exception("Suffix list file not found at %s", suffix_list_path)
        raise FileNotFoundError from err

    results["ncredir"]["unused"] = set()
    for entry in domains["ncredir"]:
        extracted = tld_extractor(entry)
        base_domain = f"{extracted.domain}.{extracted.suffix}"
        # Duplicates shouldn't be removed; One of them might still be active.
        if base_domain not in managed_domains \
                and base_domain not in results["mark"]["duplicate"] \
                and base_domain not in ignored_domains:
            if extracted.subdomain:
                logger.debug("%s is a subdomain of %s, which is not managed; Deleting.",
                             entry, f"{extracted.domain}.{extracted.suffix}")
            else:
                logger.debug("%s is not a managed domain; Deleting.", entry)
            results["ncredir"]["unused"].add(entry)

    if pretty_print:
        results_json = json.dumps(results, sort_keys=True, cls=SetToListEncoder, indent=4)
    else:
        results_json = json.dumps(results, sort_keys=True, cls=SetToListEncoder)

    logger.debug("Categorized results:\n%s", results_json)
    return results_json


def format_mm_email(wrong_ns: list, empty_ns: list,
                    unmanaged: list, duplicate: list) -> Tuple[str, str]:
    """
    Prepare email text to send about necessary MarkMonitor domain.
    """
    num_bad_domains = len(wrong_ns + empty_ns + unmanaged + duplicate)
    if num_bad_domains == 1:
        subject = f"{num_bad_domains} MarkMonitor domain require manual correction"
    else:
        subject = f"{num_bad_domains} MarkMonitor domains require manual correction"

    body = "NCMonitor, a utility for monitoring domain management anomalies, has\n" + \
           "detected domains that may require manual action.\n\n"
    if wrong_ns:
        body += "Incorrect/Incomplete NS records:\n\n"
        body += "\n".join(wrong_ns)
    if empty_ns:
        body += "\n\nNo nameserver (NS) records at all:\n\n"
        body += "\n".join(empty_ns)
    if unmanaged:
        body += "\n\nDomains that are invalid/failing registration in MarkMonitor:\n\n"
        body += "\n".join(unmanaged)
    if duplicate:
        body += "\n\nDomains found more than once in MarkMonitor (duplicates):\n\n"
        body += "\n".join(duplicate)

    body += "\n\nCorrecting these issues will allow NCMonitor to automatically\n" + \
            "provision downstream services when it next runs."

    return (subject, body)


def send_email(mailto: list, subject: str, body: str, dry_run: bool = False) -> None:
    """
    Simple function to send an email.
    """
    msg = EmailMessage()
    msg.set_content(body)
    msg["From"] = f'"NCMonitor" <{os.environ["USER"]}@{getfqdn()}>'
    msg["Subject"] = subject
    msg["To"] = ", ".join(mailto)
    msg["Auto-Submitted"] = "auto-generated"

    logger.debug(msg)
    if dry_run:
        logger.info("[DRY-RUN] Would send mail to:\n%s", msg["To"])
    else:
        logger.info("Sending mail to:\n%s", msg["To"])
        s = SMTP("localhost")
        s.send_message(msg)
        s.quit()


def submit_patch(repo, missing: list, unused: list, dry_run: bool, reviewers: list) -> None:
    """
    Submit a patch to Gerrit.
    """
    repo.hard_reset()
    if missing:
        repo.add_entries(missing)
        logger.info("%s missing domains: %s", repo, missing)
    if unused:
        repo.remove_entries(unused)
        logger.info("%s unused domains: %s", repo, unused)
    if missing or unused:
        # DNS repo doesn't stage anything to write to files; It just creates
        # symlinks.
        if hasattr(repo, "write_file"):
            repo.write_file()
        else:
            logger.debug("Skipping nonexistent %s.write_file() execution", repo.__class__.__name__)

        repo.commit()
        if dry_run:
            logger.info("[DRY-RUN] Would push %s patch", repo)
        else:
            repo.push(reviewers)


def main():
    """
    Get acme-chief, ncredir, and MarkMonitor domain information. Compare and
    report on any inconsistencies.
    """
    args = parse_args()
    setup_logging(args)
    logger.debug(args)

    # Did the user supply a config file?
    if args.config:
        config = NCMonitorConfig(args.config)
    else:
        config = NCMonitorConfig(get_conf_path())

    with (
        ACMEChiefConfig(config.config_file["acmechief"]["remote-url"],
                        config.config_file["acmechief"]["conf-path"]) as acmechief,
        DNSRepository(config.config_file["dnsrepo"]["remote-url"],
                      config.config_file["dnsrepo"]["target-zone-path"]) as dnsrepo,
        NCRedirRedirects(config.config_file["ncredir"]["remote-url"],
                         config.config_file["ncredir"]["datfile-path"]) as ncredir,
    ):

        domains = get_domains(config.config_file["markmonitor"]["username"],
                              config.config_file["markmonitor"]["password"],
                              acmechief,
                              ncredir,
                              dnsrepo,
                              args.pprint)

        gerrit_reviewers = config.config_file["gerrit"]["reviewers"]

        process_results = process_domains(
            config.nameservers,
            domains,
            config.config_file["suffix-list-path"],
            config.ignored_domains,
            args.pprint
        )
        process_results_dict = json.loads(process_results)

        domains_empty_ns = process_results_dict["mark"]["empty_ns"]
        domains_wrong_ns = process_results_dict["mark"]["wrong_ns"]
        domains_unmanaged = process_results_dict["mark"]["unmanaged"]
        domains_duplicate = process_results_dict["mark"]["duplicate"]
        logger.info("MarkMonitor domains with incorrect/incomplete NS records:\n%s",
                    domains_wrong_ns)
        logger.info("MarkMonitor domains with empty NS records:\n%s", domains_empty_ns)
        logger.info("MarkMonitor domains with invalid registration:\n%s", domains_unmanaged)
        logger.info("MarkMonitor duplicate domains:\n%s", domains_duplicate)
        if args.email_ns_issues:
            if not domains_empty_ns and not domains_wrong_ns and \
               not domains_unmanaged and not domains_duplicate:
                logger.exception("Trying to send an email without any problematic domains")

            else:
                subject, body = format_mm_email(domains_wrong_ns,
                                                domains_empty_ns,
                                                domains_unmanaged,
                                                domains_duplicate)
                send_email(gerrit_reviewers, subject, body, args.dry_run)

        if args.submit_patch:
            submit_patch(dnsrepo,
                         process_results_dict["dnsrepo"]["missing"],
                         process_results_dict["dnsrepo"]["unused"],
                         gerrit_reviewers, args.dry_run)
            submit_patch(ncredir,
                         process_results_dict["ncredir"]["missing"],
                         process_results_dict["ncredir"]["unused"],
                         gerrit_reviewers, args.dry_run)
            # Prevent hitting API limits by containing the number of any additions
            submit_patch(acmechief,
                         process_results_dict["acme"]["missing"][:15],
                         process_results_dict["acme"]["unused"],
                         gerrit_reviewers, args.dry_run)

        else:
            logger.info("Final result of domains to address:")
            print(process_results)


if __name__ == '__main__':
    main()
