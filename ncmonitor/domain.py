"""
SPDX-License-Identifier: GPL-3.0-or-later
"""


# pylint: disable-next=too-few-public-methods
class Domain:
    """
    For use with generative statements; This provides a unified way to yield
    domain information across these various modules.
    """
    def __init__(self, domain_name: str, status=None, nameservers=None):
        self.domain_name = domain_name
        self.nameservers = nameservers
        self.status = status
