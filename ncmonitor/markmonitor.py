"""
SPDX-License-Identifier: GPL-3.0-or-later
"""

import logging

import requests
from requests.exceptions import HTTPError, Timeout, TooManyRedirects

from .domain import Domain

API_URL = 'https://domains.markmonitor.com/domains/restapi/v2'
LOGIN_ENDPOINT = '/login'
LOGOUT_ENDPOINT = '/logout'
LIST_DOMAINS_ENDPOINT = '/domains'
GET_DOMAIN_ENDPOINT = '/domains/{domain_id}'

logger = logging.getLogger(__name__)


class MarkMonitorError(Exception):
    """
    Base MarkMonitor Error class.
    """


class MarkMonitorAPIError(MarkMonitorError):
    """
    MarkMonitor API Error class.
    """


class MarkMonitor:
    """
    An object for connecting to/querying MarkMonitor via the API. The only
    function this currently serves is to get the list of domains associated
    with the account: The rest are just implementation details.

    Example API responses can be viewed at:
    https://domains.markmonitor.com/domains/setup/restapi/#/Domain/findDomainById
    """
    def __init__(self, username, password, api_url=API_URL):
        self.username = username
        self.password = password
        self.api_url = api_url
        self.headers = {'Content-Type': 'application/json', 'User-Agent': 'ncmonitor/1.4.0a1'}
        self.session = None

    def __enter__(self):
        self._login()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.session:
            self._logout()

    def _get_url(self, endpoint, **kwargs):
        return f"{self.api_url}{endpoint.format(**kwargs)}"

    def _request(self, url, **kwargs):
        session = kwargs.get('session', self.session)
        json = kwargs.get('json', None)

        try:
            if json:
                logger.debug('Sending POST request to %s', url)
                response = session.post(url, json=json)
            else:
                logger.debug('Sending GET request to %s', url)
                response = session.get(url)
            response.raise_for_status()
            response_json = response.json()
            if response_json['responseCode'] > 1001:
                raise MarkMonitorAPIError(
                    f"Unexpected response {response_json['responseCode']}:",
                    f"{response_json['responseMessage']}"
                )
            logger.debug(response_json)
            return response_json
        except (HTTPError, Timeout, TooManyRedirects) as request_error:
            raise MarkMonitorError from request_error

    def _login(self, login_endpoint=LOGIN_ENDPOINT):
        login_url = self._get_url(login_endpoint)
        payload = {'username': self.username, 'password': self.password}
        session = requests.Session()
        session.headers.update(self.headers)
        self._request(login_url, session=session, json=payload)
        self.session = session

    def _logout(self, logout_endpoint=LOGOUT_ENDPOINT):
        logout_url = self._get_url(logout_endpoint)
        self._request(logout_url)
        self.session = None

    def _get_domain(self, domain_id, get_domain_endpoint=GET_DOMAIN_ENDPOINT):
        domain_url = self._get_url(get_domain_endpoint, domain_id=domain_id)
        results_json = self._request(domain_url)
        # We don't need to know everything about the domain.
        domain_name = results_json["domainName"]
        domain_status = results_json["status"]
        try:
            domain_nses = results_json["nameservers"]
            domain_nses.sort()
        except KeyError:
            domain_nses = None

        return Domain(domain_name, domain_status, domain_nses)

    def get_domain_list(self, list_domains_endpoint=LIST_DOMAINS_ENDPOINT, _=None):
        """
        Reach out to MarkMonitor via the API, get domains and their associated
        nameservers, then combine/yield the domains.
        """
        list_url = self._get_url(list_domains_endpoint)
        list_json = self._request(list_url)
        for listed_domain in list_json['domainList']:
            domain_id = listed_domain['domainId']
            domain = self._get_domain(domain_id)
            yield domain
