"""
SPDX-License-Identifier: GPL-3.0-or-later
"""

import binascii
import logging
from os import remove, symlink
from pathlib import Path
import re
import tempfile
import yaml  # type: ignore

from git import Actor, Repo
from git.exc import GitError
from git.index.base import IndexFile

from .domain import Domain

REDIRECT_COMMANDS = ('funnel', 'rewrite', 'override')
CERT_BLOCK_KEYNAME = "certificates::acme_chief"
COMMIT_ACTOR = Actor("NCMonitor", "sre-traffic+ncmonitor@wikimedia.org")
# NB Component will be prepended to the subject line
COMMIT_MSG = """Automated MarkMonitor domain sync

This is an automated patch submission from ncmonitor, a utility that
syncs MarkMonitor domain registrations with downstream WMF services.
"""
NON_CANONICAL_BLOCK_PREFIX = "non-canonical-redirect-"

logger = logging.getLogger(__name__)


class GitRepositoryError(Exception):
    """
    Base exception class.
    """


class GitRepository:
    """
    Parent class: Clone a Git repository locally.
    """
    def __init__(self, url, depth=1):
        self.url = url
        self.depth = depth
        self.temp_directory = None
        logger.debug("Temporary directory for repo is %s", self.temp_directory)
        self.repo = None
        self.staging_path = None

    def __enter__(self):
        self.temp_directory = tempfile.TemporaryDirectory(prefix="ncmonitor_")
        try:
            logger.debug("Cloning %s in %s...", self.url, self.temp_directory.name)
            self.repo = Repo.clone_from(self.url, self.temp_directory.name, depth=self.depth)
        except (OSError, GitError) as clone_error:
            raise GitRepositoryError from clone_error
        logger.debug("Temporary directory for repo is %s", self.temp_directory)
        # By default, stage the whole repo when committing
        self.staging_path = self.temp_directory.name
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if Path(self.temp_directory.name).is_dir():
            logger.debug("Deleting %s", self.temp_directory.name)
            self.temp_directory.cleanup()

    def _sort_file(self, fname: Path) -> None:
        """
        Sort the given file alphabetically.

        NB This ignores comments and has no smarts. Any comments in the middle
        of a file will be rendered useless as they'll be pushed to the top of
        the file.
        """
        entries = []
        final_lines = []
        with open(fname, mode="r", encoding="utf-8") as file:
            for line in file:
                if line.startswith('#'):
                    final_lines.append(line.strip())
                else:
                    entries.append(line.strip())

        # Don't count symbols so that similar domains are groups together.
        entries.sort(key=lambda x: re.sub('[^A-Za-z0-9_]+', '', x))

        for entry in entries:
            final_lines.append(entry.strip())

        logger.debug("Sorted file results (without joined newlines):\n%s", final_lines)
        with open(fname, mode="w", encoding="utf-8") as file:
            file.write('\n'.join(final_lines) + "\n")

    def _gen_change_id(self) -> str:
        """
        Generate a Change-ID for Gerrit.
        """
        with open("/dev/urandom", "rb") as fd:
            data = fd.read(20)
        cid = binascii.hexlify(data).decode('iso8859-1')
        logger.debug("Generated Gerrit change ID: %s", cid)
        return f'I{cid}'

    def commit(self) -> None:
        """
        Stage and then commit a path.
        """
        try:
            # GitPython does not provide an easy way to output a diff and we
            # already have a repo cloned. Might as well make it simple and
            # commit.
            class_name = self.__class__.__name__
            msg = f"{class_name}: " + COMMIT_MSG + "\nChange-Id: " + self._gen_change_id() + "\n"
            self.repo.index.add(self.staging_path)
            assert self.repo.is_dirty(self.staging_path)
            self.repo.index.commit(message=msg, author=COMMIT_ACTOR, committer=COMMIT_ACTOR)
            logger.debug("Patch summary:\n%s", self.repo.git.show())
            logger.debug(self.repo.git.diff("HEAD^"))
        except AssertionError as dirty_check_err:
            raise GitRepositoryError(
                f"{self.__class__.__name__} repo has no changes and yet is trying to commit!"
            ) from dirty_check_err

    def hard_reset(self) -> None:
        """
        Reset the Git repo; Useful before preparing a patch.
        """
        logger.debug("Forcefully checking out %s", self.temp_directory.name)
        IndexFile(self.repo).checkout(force=True)

    def push(self, reviewers: list[str]) -> None:
        """
        Push prepared patch to the repository's configured origin.
        """
        refspec = f"HEAD:refs/for/{self.repo.active_branch}%topic=ncmonitor"
        if reviewers:
            refspec = refspec + ",r=" + ",r=".join(reviewers)
        try:
            self.repo.git.push("origin", refspec)
        except (OSError, GitError) as push_error:
            raise GitRepositoryError from push_error


class DNSRepository(GitRepository):
    """
    Clone the DNS repository locally and parse the domains listed.
    """
    def __init__(self, url, zonefile_path, depth=1):
        super().__init__(url, depth)
        self.zonefile_path_rel = Path(zonefile_path)
        self.zonefile_path_full = None
        self.zonefile_dir_full = None

    def __enter__(self):
        super().__enter__()
        self.zonefile_path_full = Path(self.temp_directory.name).joinpath(self.zonefile_path_rel)
        self.zonefile_dir_full = self.zonefile_path_full.parent
        self.staging_path = self.zonefile_dir_full
        return self

    def get_domain_list(self):
        """
        Parse through ncredir's listed zones in the DNS repository and yield
        values back only if they symlink to the configured zone file.
        """
        if not self.zonefile_dir_full.is_dir():
            raise GitRepositoryError("Zone file's directory doesn't exist")

        if not self.zonefile_path_full.is_file():
            raise GitRepositoryError("ncredir zone doesn't exist")

        for p in self.zonefile_dir_full.iterdir():
            # Things we don't want to touch, such as broken symlink or internal
            # names.
            if p.is_dir() \
               or "parking" in p.name \
               or p.name == "wmnet" \
               or p.name.split(".")[-1] == "arpa" \
               or not p.exists():
                continue

            yield Domain(p.name)

    def add_entries(self, entries: list) -> None:
        """
        dnsrepo-specific file writing for new entries.
        """
        for entry in entries:
            entry_path = self.zonefile_dir_full.joinpath(entry)
            if not entry_path.exists():
                symlink(self.zonefile_path_full.name, entry_path)

    def remove_entries(self, entries: list) -> None:
        """
        dnsrepo-specific file writing for new entries.
        """
        links = [s.name for s in self.zonefile_dir_full.iterdir()]
        logger.debug("templates directory contents: %s", links)

        for entry in entries:
            logger.debug("Removing entry %s", entry)
            if entry in links:
                remove(self.zonefile_dir_full.joinpath(entry).absolute())


class ACMEChiefConfig(GitRepository):
    """
    Clone the Puppet repository and parse acme-chief's configuration to extract
    (and then yield) domain names and associated nameservers.
    """
    def __init__(self, url, conf_path, depth=1):
        super().__init__(url, depth)
        self.active_cert_block = None
        self.conf_path_full = None
        self.conf_path_rel = conf_path
        self.working_file = None

    def __enter__(self):
        super().__enter__()
        self.conf_path_full = Path(self.temp_directory.name).joinpath(self.conf_path_rel).resolve()
        self.staging_path = self.conf_path_full
        with open(self.conf_path_full, mode="r", encoding="utf-8") as file:
            try:
                self.working_file = yaml.safe_load(file.read())
            except FileNotFoundError as err:
                raise GitRepositoryError(
                    f"acme-chief's certificates file not found at {self.conf_path_rel}"
                ) from err
            except (OSError, yaml.error.YAMLError) as yaml_error:
                raise GitRepositoryError('Invalid acme-chief config') from yaml_error
            self.active_cert_block = self._get_last_cert_block_name()
        return self

    def _get_last_cert_block_name(self) -> str:
        """
        Parse working copy of the config and discover the block with the latest
        name (assumed to be the best chance at finding a place to append rather
        than create a new certificate).
        """
        cand = []
        for key in self.working_file[CERT_BLOCK_KEYNAME]:
            if NON_CANONICAL_BLOCK_PREFIX in key:
                num = key.split('-')[-1]
                cand.append(int(num))

        highest_num = max(cand)
        return f"{NON_CANONICAL_BLOCK_PREFIX}{highest_num}"

    def _get_number_snis(self, cert_block: str) -> int:
        """
        Calculate how many SNI entries there are in a specified certificate
        configuration block. Useful for determining if it's time to create a
        new block.
        """
        return len(self.working_file[CERT_BLOCK_KEYNAME][cert_block]["SNI"])

    def _create_new_cert_block(self, block_name: str) -> None:
        """
        Create a new block, likely because there's not anywhere else for added
        domains to be placed.
        """
        self.working_file[CERT_BLOCK_KEYNAME][block_name] = {}

        self.working_file[CERT_BLOCK_KEYNAME][block_name]["staging_time"] = 604800
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["challenge"] = "dns-01"
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["authorized_regexes"] = [
            r"^ncredir[0-9]{4,}\..*\.wmnet$"
        ]
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["prevalidate"] = "true"
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["skip_invalid_snis"] = "true"
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["CN"] = str()
        self.working_file[CERT_BLOCK_KEYNAME][block_name]["SNI"] = []

    def get_domain_list(self):
        """
        Parse Puppet's hieradata for acme-chief certificate domains.
        """
        domain_list = set()
        try:
            with open(self.conf_path_full, encoding="utf-8") as config_file:
                config = yaml.safe_load(config_file.read())
                if CERT_BLOCK_KEYNAME not in config:
                    raise GitRepositoryError('Invalid acme-chief config')
                for cert_name, cert_config in config[CERT_BLOCK_KEYNAME].items():
                    if NON_CANONICAL_BLOCK_PREFIX not in cert_name:
                        continue
                    # No need to add the CN to the list since it should be
                    # duplicated in the SNI list.
                    for sni in cert_config["SNI"]:
                        domain_list.add(sni.lstrip('*.'))

        except FileNotFoundError as err:
            raise GitRepositoryError(
                f"acme-chief's certificates file not found at {self.conf_path_rel}"
            ) from err
        except (OSError, yaml.error.YAMLError) as yaml_error:
            raise GitRepositoryError("Invalid acme-chief config") from yaml_error

        for domain_name in domain_list:
            yield Domain(domain_name)

    def add_entries(self, entries: list) -> None:
        """
        acme-chief-specific file writing for new entries.
        """
        for entry in entries:
            # Don't overfill the current active block.
            if self._get_number_snis(self.active_cert_block) >= 39:
                active_cert_block_num = int(self.active_cert_block.rsplit("-", maxsplit=1)[-1])
                self.active_cert_block = NON_CANONICAL_BLOCK_PREFIX + str(active_cert_block_num + 1)
                self._create_new_cert_block(self.active_cert_block)
            if not self.working_file[CERT_BLOCK_KEYNAME][self.active_cert_block]["CN"]:
                # There's no specific CN we need to define so just grab anything.
                self.working_file[CERT_BLOCK_KEYNAME][self.active_cert_block]["CN"] = entry

            self.working_file[CERT_BLOCK_KEYNAME][self.active_cert_block]["SNI"].append(entry)
            self.working_file[CERT_BLOCK_KEYNAME][self.active_cert_block]["SNI"].append(
                f"*.{entry}")

    def remove_entries(self, entries: list) -> None:
        """
        acme-chief-specific file writing for new entries.
        """
        for block in self.working_file[CERT_BLOCK_KEYNAME]:
            for entry in entries:
                # TODO: If we want to remove an entry that's a CN this will
                # explode. We need to "promote" an SNI to CN if that's the
                # case.
                if entry in self.working_file[CERT_BLOCK_KEYNAME][block]["SNI"]:
                    logger.debug("Removing %s from %s", entry, block)
                    self.working_file[CERT_BLOCK_KEYNAME][block]["SNI"].remove(entry)
                if f"*.{entry}" in self.working_file[CERT_BLOCK_KEYNAME][block]["SNI"]:
                    logger.debug("Removing %s from %s", entry, block)
                    self.working_file[CERT_BLOCK_KEYNAME][block]["SNI"].remove(f"*.{entry}")

    def write_file(self) -> None:
        """
        Write the working file to disk.
        """
        with open(self.conf_path_full, mode="w+", encoding="utf-8") as file:
            yaml.safe_dump(self.working_file, file, sort_keys=False)


class NCRedirRedirects(GitRepository):
    """
    Clone the Puppet repository locally and parse through ncredir's list of
    domains to redirect.
    """
    def __init__(self, url, datfile_path_rel, depth=1):
        super().__init__(url, depth)
        self.datfile_path_full = None
        self.datfile_path_rel = datfile_path_rel
        self.working_file = None

    def __enter__(self):
        super().__enter__()
        self.datfile_path_full = Path(self.temp_directory.name).joinpath(
                                      self.datfile_path_rel).resolve()
        self.staging_path = self.datfile_path_full

        try:
            with open(self.datfile_path_full, mode="r", encoding="utf-8") as file:
                self.working_file = file.read().split('\n')
        except FileNotFoundError as err:
            raise GitRepositoryError(
                f"ncredir datfile not found at {self.datfile_path_rel}"
            ) from err
        return self

    def get_domain_list(self):
        """
        Parse through ncredir's redirects data file and extract/yield
        redirection from non-canonical to canonical domains.
        """
        domain_list = set()
        try:
            with open(self.datfile_path_full, encoding="utf-8") as config_file:
                config = config_file.read()
                for config_line in config.splitlines():
                    line = config_line.strip()
                    if len(line) == 0 or line.startswith('#'):
                        continue
                    fields = line.split('\t')
                    if fields[0] not in REDIRECT_COMMANDS:
                        continue
                    domain_list.add(fields[1].lstrip('*').lstrip('.'))
        except FileNotFoundError as err:
            raise GitRepositoryError(
                f"ncredir datfile not found at {self.datfile_path_rel}"
            ) from err
        except OSError as config_error:
            raise GitRepositoryError('Invalid nc_redirects.dat') from config_error

        for domain_name in domain_list:
            yield Domain(domain_name)

    def add_entries(self, entries: list) -> None:
        """
        Add entries to the working copy of the configuration file. We can't
        read minds so it's all going to the same place.
        """
        for entry in entries:
            self.working_file.append(f"rewrite\t*{entry}\thttps://www.wikimedia.org")

    def remove_entries(self, entries: list) -> None:
        """
        Remove entries to the working copy of the configuration file.
        """
        to_rm = []
        entries_re_or = "|".join(entries)
        # Match *example.com, *.example.com, and example.com, the three valid
        # entry syntaxes.
        re_str = rf"^rewrite\t+(\*)?(\.)?({entries_re_or})\t"
        logger.debug("Regex in use: %s", re_str)
        for line in self.working_file:
            if re.compile(re_str).match(line):
                to_rm.append(line)

        for entry in to_rm:
            logger.debug("'%s' matches. Removing", entry)
            self.working_file.remove(entry)

    def write_file(self) -> None:
        """
        Write the working file to disk.
        """
        self._sort_file(self.datfile_path_full)
        with open(self.datfile_path_full, mode="w+", encoding="utf-8") as file:
            file.write('\n'.join(self.working_file) + "\n")
