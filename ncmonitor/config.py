"""
SPDX-License-Identifier: GPL-3.0-or-later

Retrieve configuration for use with the rest of execution.

Read the ncmonitor(5) manpage for an overview of configuration.
"""

from logging import getLogger
import os
from pathlib import Path
import re
import stat

from yaml import safe_load

DEFAULT_CONFIG_PATH = '/etc/ncmonitor/ncmonitor.yaml'
VALID_HOSTNAME_RE = r'^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$'

logger = getLogger(__name__)


class NCConfigError(Exception):
    """
    Base exception class.
    """


def is_valid_hostname(hostname: str) -> bool:
    """
    Rough validatation of the configured domain name.
    """
    logger.debug("Validating hostname %s", hostname)
    compiled = re.compile(VALID_HOSTNAME_RE, re.IGNORECASE)
    is_valid = bool(compiled.match(hostname))
    logger.debug("Does %s match domain validation regex? %s", hostname, is_valid)
    return is_valid


# pylint: disable-next=too-few-public-methods
class NCMonitorConfig:
    """
    Represents a configuration for ncmonitor
    """

    def __init__(self, path: str = DEFAULT_CONFIG_PATH) -> None:
        self.config_file = {}  # type: ignore
        self.path = path
        self.nameservers = [str]
        self.reviewers = [str]

        self._load_config(self.path)

    def _load_config(self, load_path: str) -> None:
        """
        Load a configuration file from the specified filename.
        """
        logger.debug("Loading configuration file %s.", load_path)
        # We have credentials in this file
        if self._is_valid_mode():
            with open(load_path, encoding='utf-8') as conf_file:
                self.config_file = safe_load(conf_file)
        else:
            logger.exception("%s is world-readable. Exiting", self.path)
            raise NCConfigError

        try:
            self.nameservers.clear()
            for host in self.config_file["nameservers"]:
                if is_valid_hostname(host):
                    self.nameservers.append(host)
                else:
                    logger.exception("%s is not a valid hostname.", host)
                    raise NCConfigError
            logger.debug("Configured nameservers: %s", self.nameservers)

            self.reviewers.clear()
            for email in self.config_file["gerrit"]["reviewers"]:
                if "@" in email:  # Very sophisticated email checking
                    self.reviewers.append(email)
                else:
                    logger.exception("%s is not a valid email address.", email)
                    raise NCConfigError
            logger.debug("Configured reviewers: %s", self.reviewers)

            if self.config_file["gerrit"]["ssh-key-path"] and \
               Path(self.config_file["gerrit"]["ssh-key-path"]).is_file():
                os.environ["GIT_SSH_COMMAND"] = "ssh -i "+self.config_file["gerrit"]["ssh-key-path"]

            try:
                self.ignored_domains = self.config_file["markmonitor"]["ignored-domains"]
            except KeyError:
                self.ignored_domains = []

            assert any(self.config_file["acmechief"]["conf-path"])
            assert any(self.config_file["acmechief"]["remote-url"])
            assert any(self.config_file["dnsrepo"]["remote-url"])
            assert any(self.config_file["dnsrepo"]["target-zone-path"])
            assert any(self.config_file["markmonitor"]["password"])
            assert any(self.config_file["markmonitor"]["username"])
            assert any(self.config_file["ncredir"]["datfile-path"])
            assert any(self.config_file["ncredir"]["remote-url"])
            assert any(self.config_file["suffix-list-path"])

        except (KeyError, TypeError) as exc:
            logger.exception(
                "Invalid config. See the ncmonitor(5) manpage for details on configuration."
            )
            raise NCConfigError from exc

        except FileNotFoundError as exc:
            logger.exception("SSH key not found at %s", self.config_file["gerrit"]["ssh-key-path"])
            raise NCConfigError from exc

    def _is_valid_mode(self) -> bool:
        """
        Validate the passed-in file's permissions mode to make sure it is not
        world-readable.
        """
        conf_stat = os.stat(self.path)
        conf_mode = stat.S_IMODE(conf_stat.st_mode)
        insecure = stat.S_IMODE(conf_mode) & (stat.S_IROTH | stat.S_IWOTH | stat.S_IXOTH)
        logger.debug("File stat is %s", conf_stat)
        logger.debug("Raw file mode is %s", conf_mode)
        logger.debug("File mode is %s", oct(conf_mode))
        logger.debug("File mode considered insecure: %s", insecure)
        return not insecure
